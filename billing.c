#include <stdio.h>
#include "billing.h"
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>


int billno=1;

int check_valid_date(bill_date* ptr) {
    if(ptr->year>=1900 && ptr->year<=9999)
    {
        //check month
        if(ptr->month>=1 && ptr->month<=12)
        {
            //check days
            if((ptr->day>=1 && ptr->day<=31) && (ptr->month==1 || ptr->month==3 || ptr->month==5 || ptr->month==7 || ptr->month==8 || ptr->month==10 || ptr->month==12))
                return 1;
            else if((ptr->day>=1 && ptr->day<=30) && (ptr->month==4 || ptr->month==6 || ptr->month==9 || ptr->month==11))
                return 1;
            else if((ptr->day>=1 && ptr->day<=28) && (ptr->month==2))
                return 1;
            else if(ptr->day==29 && ptr->month==2 && (ptr->year%400==0 ||(ptr->year%4==0 && ptr->year%100!=0)))
                return 1;
            else
                return 0;
        }
        else
        {
            return 0;
        }
    }
    else
    {
        return 0;
    }  
}

void store_date(bill_date* ptr,int day, int month, int year) {
    ptr->day=day;
    ptr->month=month;
    ptr->year=year;
    ptr->hours=8;
    ptr->mins=0;
    strcpy(ptr->meridiem,"AM");
}

int countLeapYears(bill_date d) {
    int years=d.year;
    if(d.month<=2) {
        years--;
    }
    return years/4-years/100+years/400;
}

int to_return_differencedays(bill_date* start_ptr,bill_date* end_ptr) {
    int day_count=0;
    int start_day=start_ptr->day;
    int end_day=end_ptr->day;
    int start_month=start_ptr->month;
    int end_month=end_ptr->month;
    int start_year=start_ptr->year;
    int end_year=end_ptr->year;
    const int monthDays[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };\

    long int n1 = start_ptr->year * 365 + start_ptr->day;
 
    for (int i = 0; i < start_ptr->month - 1; i++)
        n1 += monthDays[i];
 
    n1 += countLeapYears(*start_ptr);
 
 
    long int n2 = end_ptr->year * 365 + end_ptr->day;
    for (int i = 0; i < end_ptr->month - 1; i++)
        n2 += monthDays[i];
    n2 += countLeapYears(*end_ptr);
 
    return (n2 - n1);

}

bool isLeap(int y)
{
	if (y%100 != 0 && y%4 == 0 || y %400 == 0)
		return true;

	return false;
}

// Given a date, returns number of days elapsed
// from the beginning of the current year (1st
// jan).
int offsetDays(int d, int m, int y)
{
	int offset = d;

	switch (m - 1)
	{
	case 11:
		offset += 30;
	case 10:
		offset += 31;
	case 9:
		offset += 30;
	case 8:
		offset += 31;
	case 7:
		offset += 31;
	case 6:
		offset += 30;
	case 5:
		offset += 31;
	case 4:
		offset += 30;
	case 3:
		offset += 31;
	case 2:
		offset += 28;
	case 1:
		offset += 31;
	}

	if (isLeap(y) && m > 2)
		offset += 1;

	return offset;
}

// Given a year and days elapsed in it, finds
// date by storing results in d and m.
void revoffsetDays(int offset, int y, int *d, int *m)
{
	int month[13] = { 0, 31, 28, 31, 30, 31, 30,
					31, 31, 30, 31, 30, 31 };

	if (isLeap(y))
		month[2] = 29;

	int i;
	for (i = 1; i <= 12; i++)
	{
		if (offset <= month[i])
			break;
		offset = offset - month[i];
	}

	*d = offset;
	*m = i;
}


void add_datefunc(bill_date* ptr,int count,bill_date* res_ptr) {
    int d1=ptr->day;
    int m1=ptr->month;
    int y1=ptr->year;

    int offset1 = offsetDays(d1, m1, y1);
	int remDays = isLeap(y1)?(366-offset1):(365-offset1);

	// y2 is going to store result year and
	// offset2 is going to store offset days
	// in result year.
	int y2, offset2;
	if (count <= remDays)
	{
		y2 = y1;
		offset2 = offset1 + count;
	}

	else
	{
		// count may store thousands of days.
		// We find correct year and offset
		// in the year.
		count -= remDays;
		y2 = y1 + 1;
		int y2days = isLeap(y2)?366:365;
		while (count >= y2days)
		{
			count -= y2days;
			y2++;
			y2days = isLeap(y2)?366:365;
		}
		offset2 = count;
	}

	// Find values of day and month from
	// offset of result year.
	int m2, d2;
	revoffsetDays(offset2, y2, &d2, &m2);
    res_ptr->day=d2;
    res_ptr->month=m2;
    res_ptr->year=y2;
    res_ptr->hours=8;
    res_ptr->mins=0;
     
}

void add_nextday(bill_date* ptr,int count,bill_date* res_ptr) {
    char days[7][256]={"MON","TUE","WED","THU","FRI","SAT","SUN"};
    count=count%7;
    int iter=0;
    while(iter<7) {
        if(strcmp(days[iter],ptr->day_of_week)==0) {
            break;
        }
        iter+=1;
    }
    while(count>0) {
        iter+=1;
        if(iter>6) {
            iter=0;
        }
        count--;
    }
    strcpy(res_ptr->day_of_week,days[iter]);
}

int check_name_exists(int a[],int check,int count) {
    for(int i=0;i<count;i++) {
        if(a[i]==check) {
            return 0;
        }
    }
    return 1;
}

void next_time(bill_date* ptr,int interval) {
    int hour;
    int sub_interval=(rand() % 12)+1;
    int temp=480+interval+sub_interval;
    hour=temp/60;
    temp=temp-(hour*60);
    strcpy(ptr->meridiem,"AM");
    if(hour>=13) {
        hour-=12;
        if(hour>=1) {
            hour+=3;
        }
        strcpy(ptr->meridiem,"PM");
    }
    if(hour==12) {
        strcpy(ptr->meridiem,"PM");
    }
    ptr->hours=hour;
    ptr->mins=temp;
}

void generate_data(bill_date* ptr) {
    float amount;
    int no_of_bills;
    no_of_bills=(rand()%21)+20;
    int row;
    if(strcmp(ptr->day_of_week,"SUN")==0 || strcmp(ptr->day_of_week,"SAT")==0) {
        no_of_bills=(rand()%21)+45;
    }
    FILE *file,*file_write;
    file_write=fopen("bill_summary.txt","a");
    
    int count=0;
    int bufferLength = 255;

    int check_Array[70];
    int array_count=0;

    int interval=(int)660/no_of_bills;

    while (count<no_of_bills)
    {
        row=(rand() % 70)+1;
        int temp_count=0;
        char buffer[255];

        if(check_name_exists(check_Array,row,array_count)) {
            next_time(ptr,(count*interval));
            check_Array[array_count]=row;
            array_count+=1;
            int id;
            char name[bufferLength];
            file=fopen("name.txt","r");
            while (fscanf(file,"%d %s",&id,name) !=EOF) {
                if(row==id) {    
                    amount=(rand() % 4901)+100;       
                    fprintf(file_write,"%02d-%02d-%04d\t%03s\t%02d:%02d\t%02s\t%02d\t%10s\t%04d\t%.2f\n",ptr->day,ptr->month,ptr->year,ptr->day_of_week,ptr->hours,ptr->mins,ptr->meridiem,id,name,billno,amount);
                    billno+=1;
                    break;
                }
            }
            fclose(file);
            count+=1;
        }
    }
    fclose(file_write);
}



void generate_intermediate_data_file(bill_date* start_ptr, bill_date *end_ptr) {
    int count_days=to_return_differencedays(start_ptr,end_ptr);
    int count=0;
    while (count<=count_days)
    {
        bill_date temp;
        add_nextday(start_ptr,count,&temp);
        if(strcmp(temp.day_of_week,"TUE")!=0) {
            add_datefunc(start_ptr,count,&temp);
            generate_data(&temp);
        }
        count+=1;
    }
}

void generate_report_day() {
    FILE *f,*fwrite;
    f=fopen("bill_summary.txt","r");
    int bufferLength = 255;

    char date[bufferLength];
    char day[bufferLength];
    char time[bufferLength];
    char meridean[bufferLength];
    char id[bufferLength];
    char name[bufferLength];
    char bill_req[bufferLength];
    char prev[bufferLength];

    float amount,final_amount=0;

    fwrite=fopen("day_report.txt","a");
    int flag=0;
    while (fscanf(f,"%s\t%s\t%s\t%s\t%s\t%s\t%s\t%f\n",date,day,time,meridean,id,name,bill_req,&amount)!=EOF)
    {
        if(strcmp(prev,date)!=0 && flag==1) {
            fprintf(fwrite,"%s\t%.2f\n",prev,final_amount);
            final_amount=0;
        }
        final_amount+=amount;
        strcpy(prev,date);
        flag=1;
    }
    fprintf(fwrite,"%s\t%.2f\n",date,final_amount);

    fclose(f);
    
}

void generate_report_name() {
    FILE *fname,*f,*fwrite;
    int bufferLength = 255;
    fname=fopen("name.txt","r");
    fwrite=fopen("name_report.txt","a");
    int id;
    char name[bufferLength];
    while (fscanf(fname,"%d %s",&id,name) !=EOF)
    {
        char date[bufferLength];
        char day[bufferLength];
        char time[bufferLength];
        char meridean[bufferLength];
        char id[bufferLength];
        char check_name[bufferLength];
        char bill_req[bufferLength];
        char prev[bufferLength];
    
        float amount,final_amount=0;
        
        f=fopen("bill_summary.txt","r");
        while(fscanf(f,"%s\t%s\t%s\t%s\t%s\t%s\t%s\t%f\n",date,day,time,meridean,id,check_name,bill_req,&amount) != EOF) {
            if(strcmp(check_name,name)==0) {
                final_amount+=amount;
            }
        }
        fclose(f);

        fprintf(fwrite,"%10s\t%.2f\n",name,final_amount);
    }
    fclose(fwrite);
    fclose(fname);
    
}

void sales_per_day() {
    FILE *f,*fwrite;
    int bufferLength = 255;
    fwrite=fopen("sales_per_day.txt","a");
    char days[7][256]={"MON","TUE","WED","THU","FRI","SAT","SUN"};

    char date[bufferLength];
    char day[bufferLength];
    char time[bufferLength];
    char meridean[bufferLength];
    char id[bufferLength];
    char check_name[bufferLength];
    char bill_req[bufferLength];
    char prev[bufferLength];
    
    for(int i=0;i<7;i++) {
        float amount,final_amount=0;
        f=fopen("bill_summary.txt","r");
        while(fscanf(f,"%s\t%s\t%s\t%s\t%s\t%s\t%s\t%f\n",date,day,time,meridean,id,check_name,bill_req,&amount) != EOF) {
            if(strcmp(day,days[i])==0) {
                final_amount+=amount;
            }
        }
        fclose(f);

        fprintf(fwrite,"%3s\t%.2f\n",days[i],final_amount);
    }
    fclose(fwrite);
}

void sales_per_hour() {
    FILE *f,*fwrite;
    int bufferLength = 255;
    fwrite=fopen("sales_per_hour.txt","a");

    int array_of_hours[5] = {8,9,10,11,12};
    int array_of_12[2]={12,1};
    int array_of_hourspm[7]={4,5,6,7,8,9,10};

    char date[bufferLength];
    char day[bufferLength];
    int time_hour;
    int time_min;
    char meridean[bufferLength];
    char id[bufferLength];
    char check_name[bufferLength];
    char bill_req[bufferLength];
    char prev[bufferLength];

    for(int i=0;i<4;i++) {
        float amount,final_amount=0;
        f=fopen("bill_summary.txt","r");
        while(fscanf(f,"%s\t%s\t%d:%d\t%s\t%s\t%s\t%s\t%f\n",date,day,&time_hour,&time_min,meridean,id,check_name,bill_req,&amount) != EOF) {
            if(strcmp(meridean,"AM")==0 && time_hour==array_of_hours[i]) {
                final_amount+=amount;
            }
        }
        fclose(f);
        char meridean_2[bufferLength];
        char meridean_1[bufferLength];
        strcpy(meridean_1,"AM");
        strcpy(meridean_2,"AM");
        if(array_of_hours[i+1]==12) {
            strcpy(meridean_2,"PM");
        }
        fprintf(fwrite,"%2d:00 %s - %2d:00 %s\t%.2f\n",array_of_hours[i],meridean_1,array_of_hours[i+1],meridean_2,final_amount);
    }
    for(int i=0;i<1;i++) {
        float amount,final_amount=0;
        f=fopen("bill_summary.txt","r");
        while(fscanf(f,"%s\t%s\t%d:%d\t%s\t%s\t%s\t%s\t%f\n",date,day,&time_hour,&time_min,meridean,id,check_name,bill_req,&amount) != EOF) {
            if(strcmp(meridean,"PM")==0 && time_hour==array_of_12[i]) {
                final_amount+=amount;
            }
        }
        fclose(f);
        char meridean_2[bufferLength];
        char meridean_1[bufferLength];
        strcpy(meridean_1,"PM");
        strcpy(meridean_2,"PM");
        fprintf(fwrite,"%2d:00 %s - %2d:00 %s\t%.2f\n",array_of_12[i],meridean,array_of_12[i+1],meridean,final_amount);
    }
    for(int i=0;i<6;i++) {
        float amount,final_amount=0;
        f=fopen("bill_summary.txt","r");
        while(fscanf(f,"%s\t%s\t%d:%d\t%s\t%s\t%s\t%s\t%f\n",date,day,&time_hour,&time_min,meridean,id,check_name,bill_req,&amount) != EOF) {
            if(strcmp(meridean,"PM")==0 && time_hour==array_of_hourspm[i]) {
                final_amount+=amount;
            }
        }
        fclose(f);
        char meridean_2[bufferLength];
        char meridean_1[bufferLength];
        strcpy(meridean_1,"PM");
        strcpy(meridean_2,"PM");
        fprintf(fwrite,"%2d:00 %s - %2d:00 %s\t%.2f\n",array_of_hourspm[i],meridean,array_of_hourspm[i+1],meridean,final_amount);
    }
    fclose(fwrite);
}

 
void generate_report() {
    generate_report_day();
    generate_report_name();
    sales_per_day();
    sales_per_hour();
}

