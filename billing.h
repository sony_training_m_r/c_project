struct date
{
    int day;
    int month;
    int year;
    char day_of_week[3];
    int hours;
    int mins;
    char meridiem[2];
};

typedef struct date bill_date;

int check_valid_date(bill_date* ptr);
// To determine the date constraints (Leap year or not exceding 31 days or not etc.)
// Date format dd-mm-yyyy
// Cheack leap year first
// Check month less than 12
// Check day is less than 28/30/31 depending on the year.

// int to_return_maxdays(bill_date* ptr);
// Returns the maximum day of that month
// we are not using it.

int to_return_differencedays(bill_date* start_ptr,bill_date* end_ptr);
// Returns the number of days between these two days.

int check_name_exists(int a[],int check,int count);
// To check whether name exits or not.
// We are not using it.

void add_datefunc(bill_date* ptr,int count,bill_date* res_ptr);
// To add a number of days to current days.

void add_nextday(bill_date* ptr,int count,bill_date* res_ptr);
// To write next day to current days.

void store_date(bill_date* ptr,int day, int month, int year);
// Store the date in the pointer.

void generate_data(bill_date* ptr);
// Random choice of name from <Name_Data.txt>
// Random amount generation range from 100-5000
// Output intermediate_data_file <Date_analyse.txt> 
// Format of output
// <Date><space><Name><space><Amount><New Line>

void generate_intermediate_data_file(bill_date* start_ptr, bill_date *end_ptr);
// Iterate through start date to end date to generate data 

void generate_report();
// Genrates two report file

void generate_report_day();
// 1) Per Day Output file <Day_Report.txt> (One file contains report of all days)

void generate_report_name();
// 2) Name wise Report <Name_Report.txt> (One file contains report of all names)

void next_time(bill_date* ptr,int interval);
// To Determine the next time zone.

void sales_per_day();
// To Determine the sales per day.

void sales_per_hour();
// To Determine the sales per hour.