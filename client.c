/*
Client Interface
*/

#include<stdio.h>
#include<string.h>
#include "billing.h"

int main() {
    //declaration of flag and pointer to start and end date.
    int exit_condition=1;
    bill_date start_date;
    bill_date end_date;
    int day,month,year;

    while (exit_condition)
    {
        printf("Enter the Starting Date for Report\n");
        printf("Enter the Day\n");
        scanf("%d",&day);
        printf("Enter the month\n");
        scanf("%d",&month);
        printf("Enter the year\n");
        scanf("%d",&year);
        store_date(&start_date,day,month,year);
        if(check_valid_date(&start_date)) {
            exit_condition=0;
        }
        else {
            printf("INVALID DATE please re enter\n");
        }
    }
    exit_condition=1;
    while (exit_condition)
    {
        int choice;
        printf("Chooce the Day\n1)MON\t2)TUE\t3)WED\t4)THU\t5)FRI\t6)SAT\t7)SUN\nDefault choice will be Monday(for incorrect entry)\n");
        scanf("%d",&choice);
        switch (choice)
        {
        case 1:strcpy(start_date.day_of_week,"MON");
            exit_condition=0;
            break;
        case 2:strcpy(start_date.day_of_week,"TUE");
            exit_condition=0;
            break;
        case 3:strcpy(start_date.day_of_week,"WED");
            exit_condition=0;
            break;
        case 4:strcpy(start_date.day_of_week,"THU");
            exit_condition=0;
            break;
        case 5:strcpy(start_date.day_of_week,"FRI");
            exit_condition=0;
            break;
        case 6:strcpy(start_date.day_of_week,"SAT");
            exit_condition=0;
            break;
        case 7:strcpy(start_date.day_of_week,"SUN");
            exit_condition=0;
            break;
        
        default:strcpy(start_date.day_of_week,"MON");
            exit_condition=0;
            break;
        }
    }
    exit_condition=1;
    while (exit_condition)
    {
        printf("Enter the End Date for Report\n");
        printf("Enter the Day\n");
        scanf("%d",&day);
        printf("Enter the month\n");
        scanf("%d",&month);
        printf("Enter the year\n");
        scanf("%d",&year);
        store_date(&end_date,day,month,year);
        if(check_valid_date(&end_date) && to_return_differencedays(&start_date,&end_date)>=0) {
            exit_condition=0;
        }
        else {
            printf("INVALID DATE or end date not greater than start date please re enter\n");
        }
    }
    generate_intermediate_data_file(&start_date,&end_date);
    generate_report();
    return 0;
}